/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 *
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img a src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function vsebujeSlike(besedilo){
  var slike = vrniSlike(besedilo);
  if(slike && slike.length>0) return true;
  return false
}

function vrniSlike(besedilo){
  var regex = /\bhttp.*?\.(?:jpg|gif|png)\b/gi;;

  var urls = [];
  var url;
  while(url=regex.exec(besedilo)){
    urls.push(url);
  }
  return urls;
}

function dodajSlike(vhodnoBesedilo) {

  var slike = vrniSlike(vhodnoBesedilo);
  if(!slike) return vhodnoBesedilo;

  for(var i=0; i<slike.length; i++){
    var slikaHtml = `<br><img style="width: 200px; padding-left: 20px;" src='${slike[i]}' />`
    vhodnoBesedilo += slikaHtml;
  }

  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 *
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jeSlika = vsebujeSlike(sporocilo);
  if (jeSmesko || jeSlika) {
    // Tole tuki je tak šalabajzarizem da ka je BOG odpuščov
    // grehe ni imel vmislih takih stvari
    //
    // Raje rečem da te kode nisem napisal jaz in da ne naredim predmeta
    // kot pa da bi kdo videl to kodo in mislil da je moja
    // to kodo sem našel tukaj in definitivno ni moja!
    sporocilo =
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("&lt;br&gt;<img").join("<br/><img")
               .split("png' /&gt;").join("png' />")
               .split("gif' /&gt;").join("gif' />")
               .split("jpg' /&gt;").join("jpg' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 *
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 *
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSlike(sporocilo);
  sporocilo = dodajSmeske(sporocilo);

  var sistemskoSporocilo;

  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n');
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
*
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }

 return vhod;
}

var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement;
    if(sporocilo.uporabnik){
      var vzdevek = klepetApp.getVzdevek(sporocilo.uporabnik);
      novElement = vzdevek + sporocilo.besedilo;
    }else{
      novElement = sporocilo.besedilo
    }

    $('#sporocila').append(divElementEnostavniTekst(novElement));
  });

  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }

    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    klepetApp.uporabniki = uporabniki;
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {

      var userDiv = divElementEnostavniTekst(upo);
      userDiv.attr('uporabnik', uporabniki[i]);

      userDiv.click(function() {
        var upor = $(this).attr("uporabnik");

        var sporocilo = divElementHtmlTekst("Krcam " + upor);
        if(upor != trenutniVzdevek){
          klepetApp.posljiZasebnoSporocilo(upor, "☜");
          sporocilo = "Krcam " + upor;
        }else{
          sporocilo = "Kar sebe bi kercnu?";
        }

        $('#sporocila').append(divElementHtmlTekst(sporocilo));
        return false;
      });
      $('#seznam-uporabnikov').append(userDiv);
    }
  });

  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();

  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
